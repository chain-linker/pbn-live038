---
layout: post
title: "최강야구 몬스터즈 리뷰"
toc: true
---


## 1. 프로그램 기저 정보
 최강야구는 은퇴한 야구 선수들이 고등학교, 대학교 등의 아마추어 야구 선수들과 경기하는 모습을 보여주는 프로그램입니다. 은퇴를 했어도 야구 사랑하는 마음과는 대조적으로, 떨어진 신체능력, 잃어버린 감, 거듭된 실수로 인한 정서적 응력 등 이상과 현실의 충돌이 시청자들을 조마조마하게 만듭니다. 도리어 계한 팀으로써 서로의 부족함을 채워주며, 다독이며, 응원하며 승리하고 더군다나 패배하는 모습이 감동을 줍니다. 첫 방송은 2022년 6월 6일이며, 시간은 매주 월요일 22시 30분에 JTBC 채널에서 방영됩니다.

 감독으로는 KBO 레전드 이승엽, 투수로는 심수창, 장원삼, 송승준, 유희관, 이대은, 오주원, 포수는 이홍구, 윤준호(대학생), 야수로는 박용택, 정성훈, 이택근, 정근우, 서동욱, 정의윤, 김문호, 류현인(대학생), 최수현(독립리그) 등이 출연합니다.
 출처: 최강야구 공식홈페이지 캡쳐

## 2 연출
 연출은 채널A 방송에서 ‘도시어부’, ‘강철부대’로 히트를 친 장시원 PD가 연출을 맡았습니다. 대형 뉴스거리 3사 출신은 아니지만 기획하는 프로그램마다 반응이 좋아 JTBC로 이적을 하고 더한층 큰 스케일의 작품을 연출할 이운 있는 기회를 얻은 것으로 보입니다. 모발 PD는 ‘도시어부’에서 낚시 트랜드를 이어 캐치하고 지루하다고 여겨지는 낚시와 예능을 결합하여 낚시에 대한 이미지를 타파했습니다. 코로나로 첩 활동에 제약이 있던 시기에 너른 바다에서 자유롭게 고기를 잡는 모습을 통해 시청자들에게 대리만족을 선사하며 낚시의 대중화에 일조했습니다. ‘강철부대’에서는 젊은이들의 협동과 경쟁, 한계를 돌파하는 모습들을 보여주며 감동을 선사했고, 미온적 인식이 강하던 군대라는 조직의 밝은 면을 보여준 프로그램이었습니다.
 최강야구에서도 과거에 영광을 누렸지만, 애걸복걸 앞에 무뎌진 몸을 이끌고 자신의 자리에서 최선을 다하려는 은퇴한 야구 선수들의 모습을 통해 자기의 분야에서, 가일층 나아가 우리의 일상생활 전체에서 열정을 이어나가는 아름다운 모습들을 기대하게 됩니다.

## 3. 기억에 남는 장면
 장부 기억에 남는 장면은 10화의 뜻 한가운데 이홍구 포수의 입스 장면입니다. 입스(YIPS)란 [야구입스](https://unwieldypocket.com/sports/post-00028.html) 경연, 경쟁, 심사처럼 압박이 있는 상황에서 불안 증세로 근육이 경직되어 평소에는 문제없이 반복하던 동작을 할 무망지복 없게 되는 것을 말합니다.
 발빠른 고등학생들의 계속되는 도루 시도에 송구 실책을 하게된 트라우마가 이홍구 선수에게 입스를 일으킨 것으로 보입니다. 권위자 선수 시대 수많은 경기를 치뤘지만, 은퇴 사후 예전같지 않은 신체 능력, 율동 방송이 아닌 문예 방송에서 출연, 아마추어 10대 선수들과의 경기에서 이겨야 한다는 부담이 이홍구 선수에게 엄청난 압박을 가하고 있는 것 같았습니다.
 ‘세월 앞에 비즈니스 없다’라는 말이 있는데, 이기고 싶은 마음, 잘하고 싶은 마음은 직 선수 때와 같지만 몸이 따라 거소 않고, 최선을 다했는데 좋은 결과를 얻지 못함으로 내적으로 갈등하고 좌절하는 모습이 안타깝게 보였습니다.
 이런즉 이홍수 선수를 다독이며 애정어린 조언을 아끼지 않는 송승준 선수의 모습에서 팀워크에 대해 재차 범위 등차 생각하게 되었습니다. “무조건 쎄게 꼽아뿌라” “생각하지 말고 던져라” 는 조언으로 이홍구 선수에게서 불안감을 떨어내주려 하며, “한 번에 좋아지는 건 없다. 오늘 실리 만큼 고쳐진 것 같으면 만족하고 훗날 와서 더군다나 당분간 고쳐가는 것이 인생이다”라는 말은 주식, 비트코인 등으로 일확천금을 꿈꾸는 젊은 세대에게 경종을 울리는 말로 다가왔습니다.
 출처: 최강야구 공식홈페이지 캡쳐

## 4. 총평
 마음만은 청춘인 은퇴 야구 선수들의 고군분투 이야기를 보며, ‘그래도 프로까지 뛴 선수들의 마인드는 다르다’라는 것을 느낍니다. 대충해도 출연료는 받을 것이고, 출연료가 생계에 큰 영향을 줄 것 같지 않는 기라성 같은 은퇴 선수들이 연신 승리를 갈망하며, 부단히 자신을 채찍질하는 모습이 참 멋있어 보입니다. 회차를 거듭할수록 발전하고 나이가 들어도 할 행복 있다는 성장적 사고를 증명해 많은 사람들에게 희망과 용기를 주는 모습들을 볼 행복 있기를 기대합니다.
