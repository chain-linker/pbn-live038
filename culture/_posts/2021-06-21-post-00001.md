---
layout: post
title: "만화 타입"
toc: true
---



## 

### open.kakao.com/me/monte_cms

#### ✉ 의도 및 신청은 오픈 카톡으로만 받습니다. 링크로 연락 부탁드립니다! ✉
 DM은 오류로 씹히는 일이 많고, 확인이 느립니다.
 

 

### ✓  읽어주세요 !!

 

 - 작업물의 저작권은 양도하지 않습니다. 그림에 대한 저작권은 제게 있습니다.
 - 작업물 사용 시 출처 표기(@monte_cms) or 커미션임을 밝혀주세요.
 - 작업물에 대한 도용/트레이싱/배포/리터칭/상업적 이용(방송 이용, 굿즈 제작 등)을 금지합니다.
 - 개인 사장 용도에 한하여 보정, 리사이징을 허용합니다.
 - 소장용 개인 굿즈의 경우, 제조 전 종전 연락해주신다면 괜찮습니다.
 - 중간과정(러프) 이미지 업로드를 금합니다.

 - 작업물이 샘플로 사용될 생명 있습니다.
 

 시범 관구 조인 타입이라, 금 조정이 있을 요체 있습니다.
 

 

#### ⌧  받지 않아요 !!
 퍼리, 고수위, 작품 복판 캐릭터가 아닌 사물 인계 (아이돌, 배우 등..)
 

 

#### ⟲  환불 규정
 단순 변심으로 인한 환불은 불가능합니다.​
기간 내에 완성하지 못할 경우 전액 환불해드립니다.
 역사 기간은 입금 확인 뒤 최대한 30일입니다.
컨펌 단계 이후의 수정은 추가금이 붙습니다.
 

 

#### ❏  추가금이 드는 항목
 - 입금 뒤 24시간 이내의 빠른 종결 +100%
 - 3일 내의 빠른 미말 +50%
- 복잡한 복식이나 액세서리, 소품, 정상 금왕지절 등.. (문의)
 　가져오신 저작권 free 이미지를 가공해서 배경에 넣는 것은 추가금이 들지 않습니다.
 - 페이팔 입금 +10% (수수료 값입니다 ㅠㅠ)
 

 

 

### ❏  공작 순서
 

 오픈 카톡으로 문의 및 신청 ▶ 입금 ▶ 러프 컨펌 ▶ 완성
 

 극한 기한이 없다면, 여유를 두고 작업하는 편입니다.
 원하는 날짜가 있다면 신청서 '6. 기타' 란에 기재 부탁드립니다.
 

 

 

### ✓ 타입 설명
 종 A: 회소 콘티를 제공해주시는 경우입니다. (졸라맨도 OK)

 ex) 실용례 이미지
 

유형 B: 기사 콘티를 제공해주시는 경우입니다.
 대사와 상황, 얼굴짝 등을 적어서 보내주시면, 표기 콘티를 바탕으로 콘티를 짭니다.
ex) (긴장감이 흐르는 분위기에서 페이퍼 타올을 뜯는 존)
 존: 자네의 장인이신 형편 회장님 밑에서 일하고 있지. (여유로운 태도.)
진수: (경계하는 표정으로) 여태까지 세상영문 미행한 거야?
 

유형 C: 주인장! 오마카세!
 만화 내용을 설명해주시면, 알아서 그립니다.
 외형, 의상, 성격, 말씨 등을 자세하게 묘사해주실수록 좋습니다.
 콘티비 5000원
 

 ※ 역사 순서가 다소 다릅니다! ※
 뜻 및 신청 ▶ 콘티비 입금 ▶ 콘티 컨펌+페이지 견적 확인 최종 입금 ▶러프 컨펌 ▶완성
 (콘티 컨펌 과정에서 확실히 만화 커미션으로 이어지지 않아도 괜찮습니다.)
 

 

 🜚　요구되는 자료: 캐릭터 이미지or외모 묘사, 대사(없을 아리아 생략 가능), 음경 관계, 목하 상황
 설명이 자세할수록 작업하기에 좋습니다.
 (특별한 지시 사항이 없다면 오마카세로 간주, 작업자가 알아서 그립니다.)
 

 

🜚　출판 만화 형식으로 그립니다. (작업 사이즈: 1200*1700px, 300dpi)
 - 페이지당 컷 수는 내용에 따라 다르지만, 예사 1~6컷 입니다.
 　(8컷까지도 가능하나, 가독성이 떨어질 성명 있어 작업자가 선호하지 않습니다.)
 - 요청시 스크롤 웹툰 형식으로 편집 가능합니다.
 

 🜚　가져오신 저작권 free 이미지를 가공해서 배경에 넣는 것이 가능합니다.
 🜚　추가금이 드는 항목: 빠른 최후 / 복잡한 복식이나 액세서리, 소품, 처지 서수 (문의)
 

### 

### ❏  SAMPLE
 간경 작업물이 위로 올라옵니다.
 

 

### ❏  신청 양식
 

 1. 입금자명: 초성도 괜찮습니다.
 2. 이메일: 남겨주신 이메일로 작업물을 보내드립니다.
 3. 유형, 페이지 수, 옵션: C(오마카세)의 경우, 계획 최대한 페이지 수를 적어주세요.
 ex) A, 2페이지, 3일 근체 빠른 마감
 4. 용도:
 ex) 개인 소첩 / 커뮤 로그 / 기타..
 5. 품질 자료:
 - 가상_캐릭터 이름을 남겨주시면 좋습니다. (생략 OK)
 - 외형, 헤어스타일, 의상, 성격, 말투, 특징 등..
 - 외형을 제외한 부분 오마카세 가능하지만, 수정 컨펌이 당분간 어렵거나 추가금이 발생할 복 있습니다.
 - 놓치기 쉬운 부분, 배의 써줬으면 하는 외형 특징을 적어주시면 좋습니다.
　ex) 눈꼬리 올라감(내려감), 속눈썹 짙음 유무 등..
 - 등장인물이 2인 이상일 경우, 밥값 간의 관[웹툰 추천](https://convers-dizzy.ml/culture/post-00000.html)계를 간략하게라도 적어주시면 좋습니다.
 　(ex. 연인, 혐관, 썸타는 사이, ..)
 6. 신청 내용:​
 - 유형에 맞게 그림/글 콘티나, 만화 내용을 첨부해주세요.
 - 오마카세 가능!
 

 ✁- - 치아 밑으로는 공란 악가 알아서 작업합니다! - - - - -
 

 6. 기타: 극진지두 날짜(빠른 마감이 아닐 경우)나, 기외 요구사항
 

 

#### 'Commission' 카테고리의 다른 글
